package com.oop.gui;

import com.oop.gui.utils.*;
import com.oop.connectDB;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by Huy on 7/13/2017.
 */
public class StartPanel extends JPanel implements Constant, ActionListener, Runnable, IAudio{
    private Image backgrd;
    private JTextField id;
    private JLabel decription1;
    private JLabel decription;
    private Thread music;
    private IGUI igui;
    private JPasswordField password;
    private JButton loginbutton, loginGuess, helpButton;

    public StartPanel() {
        init();
        panel();
        addComponent();

    }

    public void setIgui(IGUI igui) {
        this.igui = igui;
    }

    private void addComponent() {
        //add icon vào panel
        add(loginbutton);
        add(password);
        add(id);
        add(loginGuess);
        add(decription1);
        add(decription);
        add(helpButton);
        //tạo sự kiện click vào icon
        loginGuess.addActionListener(this);
        loginGuess.setActionCommand("guess");
        helpButton.addActionListener(this);
        helpButton.setActionCommand("help");
        loginbutton.addActionListener(this);
        loginbutton.setActionCommand("admin");
        music = new Thread(this);
        MyAudio.playNhac(NHACNEN,true);
//        music.start();
    }

    private void panel() {
        //tạo icon Help
        Icon help = new ImageIcon(getClass().getResource("/image/button_help.png"));
        Image helpImange = ImageUtils.getImage("/image/button_help (1).png");
        Icon helpClicked = new ImageIcon(helpImange.getScaledInstance
                (help.getIconWidth(), help.getIconHeight(), Image.SCALE_DEFAULT));
        helpButton = new ButtonUtils(helpClicked, help, helpClicked,
                help.getIconWidth(), help.getIconHeight(), 35, 35, false);
        //tạo icon Get started
        Icon guess = new ImageIcon(getClass().getResource("/image/button_enter.png"));
        Image guess1 = ImageUtils.getImage("/image/button_guess.png");
        Icon guessLogin = new ImageIcon(guess1.getScaledInstance(guess.getIconWidth(),
                guess.getIconHeight(), Image.SCALE_DEFAULT));
        loginGuess = new ButtonUtils(guessLogin, guess, guess, guess.getIconWidth(),
                guess.getIconHeight(), WIDTH_GUI / 2 - guess.getIconWidth() / 2,
                (int) (HEIGH_GUI * 0.75), false);
        //icon đăng nhập quyền admin
        Image image = ImageUtils.getImage("/image/button_login1.png");
        Icon loginIcon = new ImageIcon(image.getScaledInstance(WIDTH_BUTTON, HEIGH_BUTTON, Image.SCALE_DEFAULT));
        Image image1 = ImageUtils.getImage("/image/button_login2.png");
        Icon loginClicked = new ImageIcon(image1.getScaledInstance(WIDTH_BUTTON, HEIGH_BUTTON, Image.SCALE_DEFAULT));
        Image image2 = ImageUtils.getImage("/image/button_loginclicked.png");
        Icon loginExit = new ImageIcon(image2.getScaledInstance(WIDTH_BUTTON, HEIGH_BUTTON, Image.SCALE_DEFAULT));
        loginbutton = new ButtonUtils(loginIcon, loginClicked, loginExit, loginExit.getIconWidth(),
                loginExit.getIconHeight(),
                900, 65, false);

        //Jlabel password, user name
        decription1 = new JLabel();
        decription1.setFont(new Font(Font.DIALOG, Font.BOLD, 15));
        decription1.setText("Username");
        decription1.setSize(150, 22);
        decription1.setLocation(820, 10);
        decription = new JLabel();
        decription.setFont(new Font(Font.DIALOG, Font.BOLD, 15));
        decription.setText("Password");
        decription.setForeground(Color.WHITE);
        decription1.setForeground(Color.WHITE);
        decription.setSize(150, 22);
        decription.setLocation(820, 30);
        // tạo nơi điền password, id
        id = new JTextField();
        id.setLocation(900, 10);
        id.setSize(150, 20);
        id.setText("");
        id.setBackground(Color.cyan);
        id.setFont(new Font(Font.DIALOG, Font.PLAIN, 14));
        password = new JPasswordField();
        password.setLocation(900, 35);
        password.setSize(150, 20);
        password.setText("");
        password.setBackground(Color.cyan);
    }

    private void init() {
//        setOpaque(true);
        setLayout(null);
//        setFocusable(true);
        backgrd = ImageUtils.getImage("/image/Backgroud.jpg");
    }

    private boolean check(String username, char[] password) {
        if (password.length == 0) return false;
        StringBuilder pass = new StringBuilder();
        for (int i = 0; i < password.length; i++) {
            pass.append(password[i]);
        }
        return connectDB.check(username, pass.toString());
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.drawImage(backgrd, 0, 0, null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        //thực hiện hành động khi click vào button
        switch (command) {
            case "admin":
                if (id.getText().length() == 0) {
                    JOptionPane.showMessageDialog(null, "Vui long nhap username");
                    break;
                }
                if (password.getPassword().length == 0) {
                    JOptionPane.showMessageDialog(null, "Vui long nhap password");
                    break;
                }
                if (check(id.getText(), password.getPassword())) {
                    JOptionPane.showMessageDialog(null, "Login success");
                    break;
                } else
                    JOptionPane.showMessageDialog(null, "Login fail ");
                break;
            case "guess":
                MyAudio.playNhac(HELP_MUSIC,false);
                igui.showPlayPanel();
                break;
            case "help":
//                MyAudio.playNhac(HELP_MUSIC,false);
                igui.showHelpPanel();
                break;
        }
    }

    @Override
    public void run() {


        while (true){

            try {
//                MyAudio.playNhac(HELP_MUSIC,true);
                music.sleep(32000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
//
//        while (true){
//
//            try {
//                MyAudio.playNhac(HELP_MUSIC,true);
//                music.sleep(32000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
    }
    public void stopMusic(){

    }
}

