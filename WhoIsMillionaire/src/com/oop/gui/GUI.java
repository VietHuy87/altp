package com.oop.gui;

import com.oop.gui.utils.Constant;
import com.oop.gui.utils.IAudio;
import com.oop.gui.utils.IGUI;
import com.oop.gui.utils.MyAudio;

import javax.swing.*;

/**
 * Created by Huy on 7/13/2017.
 */
public class GUI extends JFrame implements Constant, IAudio {
    private StartPanel startPanel;
    private PlayPanel playPanel;
    private HelpPanel helpPanel;

    public GUI() {
        intGUI();
        initComponents();
        addComponents();

    }

    private void addComponents() {
        add(startPanel);
//        add(helpPanel);
//        helpPanel.setVisible(false);
//        helpPanel.setVisible(false);
        startPanel.setVisible(true);

    }

    private void initComponents() {
        playPanel = new PlayPanel();
        startPanel = new StartPanel();
        helpPanel = new HelpPanel();
        IGUI igui = new IGUI() {
            @Override
            public void showPlayPanel() {
                play();
            }

            @Override
            public void showMenuPanel() {
                helpToMenu();
            }

            @Override
            public void resetPlayPanel() {
                playToMenu();
            }

            @Override
            public void showHelpPanel() {
                help();
            }

            @Override
            public void exit() {
                System.exit(0);
            }
        };
        startPanel.setIgui(igui);
        helpPanel.setIgui(igui);
        playPanel.setIgui(igui);
    }

    private void playToMenu() {//quay lại startpanel từ play panel
        remove(playPanel);
        MyAudio.playNhac(PLAY_MUSIC, false);
        MyAudio.playNhac(NHACNEN, true);
        startPanel.setVisible(true);
    }

    private void helpToMenu() { // quay lại startpanel từ help panel
        remove(helpPanel);
        add(startPanel);
        MyAudio.playNhac(HELP_MUSIC, false);
        MyAudio.playNhac(NHACNEN, true);
        startPanel.setVisible(true);

    }

    private void help() { //vào help panel
        add(helpPanel);
//        remove(startPanel);
        MyAudio.playNhac(NHACNEN, false);
        MyAudio.playNhac(HELP_MUSIC, true);
        startPanel.setVisible(false);
        helpPanel.setVisible(true);
        remove(startPanel);
    }

    private void intGUI() {
        setResizable(false);
        setTitle("Who Wants To Be A Millionaire?");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(WIDTH_GUI, HEIGH_GUI);
        setIconImage(new ImageIcon(this.getClass().getResource("/image/icon.png")).getImage());
        setLocationRelativeTo(null);
    }

    public void play() { // vào play panel
        add(playPanel);
        MyAudio.playNhac(NHACNEN, false);
        MyAudio.playNhac(PLAY_MUSIC, true);
        startPanel.setVisible(false);
    }
}
