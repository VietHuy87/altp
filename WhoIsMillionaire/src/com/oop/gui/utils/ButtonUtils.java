package com.oop.gui.utils;

//import com.t3h.pikachu.gui.common.IAudio;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by Huy on 2/8/2017.
 */
public class ButtonUtils extends JButton implements IAudio {
    public ButtonUtils(Icon icon1, Icon iconclick, Icon clicked, int wbutton, int hbutton, int xbutton, int ybutton, boolean border) {
        setIcon(icon1);
        setBorderPainted(border);
        setContentAreaFilled(false);
        setSize(wbutton, hbutton);
        setLocation(xbutton, ybutton);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                setIcon(iconclick);

            }
            @Override
            public void mouseClicked(MouseEvent e) {

                setIcon(clicked);

            }

            @Override
            public void mouseExited(MouseEvent e) {
                setIcon(icon1);

            }
        });
    }
}
