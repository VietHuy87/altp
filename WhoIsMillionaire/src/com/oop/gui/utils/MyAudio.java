package com.oop.gui.utils;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Huy on 8/1/2017.
 */
public class MyAudio {
    //    private AudioStream audioStream;

//    private AudioStream audioStream;

    private static AudioStream audioStream;

    public static void playNhac(String path, boolean run) {
        boolean isRun = run;
        path = MyAudio.class.getResource(path).getPath();
        path = path.replaceAll("%2E", ".");
        path = path.replaceAll("%23", "#");
        path = path.replaceAll("%20", " ");
        File file = new File(path);

        try {
            InputStream inputStream = new FileInputStream(file);

//            audioStream = new AudioStream(inputStream);
            if(isRun) {
                audioStream = new AudioStream(inputStream);
                AudioPlayer.player.start(audioStream);
            }
            else if(!isRun){
                if (audioStream != null){
                    AudioPlayer.player.stop(audioStream);
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
