package com.oop.gui.utils;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Huy on 7/13/2017.
 * class chứa lối tắt tới địa chỉ ảnh
 */
public class ImageUtils {
    public static Image getImage(String path) {
        return new ImageIcon(ImageUtils.class.getResource(path)).getImage();
    }
}
