package com.oop.gui.utils;

/**
 * Created by Huy on 7/30/2017.
 * cho phép class con gọi hàm tại GUI
 */
public interface IGUI {
    void showPlayPanel();
    void showMenuPanel();
    void resetPlayPanel();
    void showHelpPanel();
    void exit();
}
