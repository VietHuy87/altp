package com.oop.gui.utils;

/**
 * Created by Huy on 8/1/2017.
 */
public interface IAudio {
    String NHACNEN = "/audio/outro.wav";
    String CLICK = "/audio/click1.wav";
    String PLAY_MUSIC = "/audio/waiting.wav";
    String LOSE = "/audio/lose.wav";
    String HELP_MUSIC = "/audio/intro.wav";
    String PASS_LEVEL = "/audio/win.wav";
}
