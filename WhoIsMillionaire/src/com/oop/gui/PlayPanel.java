package com.oop.gui;

import com.oop.gui.utils.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by Huy on 7/30/2017.
 */
public class PlayPanel extends JPanel implements Constant, ActionListener, MouseListener, IAudio {
    private Image bg;
    private IGUI igui;
    private JButton back;
    private JButton question;

    public PlayPanel() {
        init();
        components();
        addComponent();
    }

    private void addComponent() {
        add(back);
        back.addActionListener(this);
        back.setActionCommand("back");
        addMouseListener(this);
    }

    private void components() {
//Tạo icon quay trở lại startpanel
        Icon backbutton = new ImageIcon(getClass().getResource("/image/button_back.png"));
        Image clickedback = ImageUtils.getImage("/image/button_back1.png");
        Icon cliked = new ImageIcon(clickedback.getScaledInstance(backbutton.getIconWidth(), backbutton.getIconHeight(), Image.SCALE_DEFAULT));
        back = new ButtonUtils(backbutton, cliked, backbutton, backbutton.getIconWidth()
                , backbutton.getIconHeight(), 65, 35, false);

    }

    public void setIgui(IGUI igui) {
        this.igui = igui;
    }

    private void init() {
        setLayout(null);
        bg = ImageUtils.getImage("/image/playbg.png");
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.drawImage(bg, 0, 0, null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String id = e.getActionCommand();
        switch (id) {
            case "back":
                igui.resetPlayPanel();
                MyAudio.playNhac(HELP_MUSIC, true);
                break;
        }
    }

    //vị trí click chuột, ông tạo jlabel câu trả lời vào các vị trí, t bắt sự kiện ở đây
    @Override
    public void mouseClicked(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        if (x > 160 && x < 520 && y > 490 && y < 535) { //click vào đáp án A
            System.out.println("cau a");
            int sure = JOptionPane.showConfirmDialog(this, "Are you sure??",
                    "Confirm", JOptionPane.YES_NO_OPTION);
            if (sure == JOptionPane.YES_OPTION) {
                MyAudio.playNhac(PLAY_MUSIC, false);
                MyAudio.playNhac(PASS_LEVEL, true);
            } else {
                MyAudio.playNhac(PLAY_MUSIC, false);
                MyAudio.playNhac(LOSE, true);
            }

        } else if (x > 550 && x < 900 && y > 490 && y < 535) { // đáp án B
            System.out.println("Cau b");
            int sure = JOptionPane.showConfirmDialog(this, "Are you sure??",
                    "Confirm", JOptionPane.YES_NO_OPTION);
            if (sure == JOptionPane.YES_OPTION) {
                MyAudio.playNhac(PLAY_MUSIC, false);
                MyAudio.playNhac(PASS_LEVEL, true);
            } else {
                MyAudio.playNhac(PLAY_MUSIC, false);
                MyAudio.playNhac(LOSE, true);
            }
        } else if (x > 160 && x < 520 && y > 545 && y < 585) { // Đáp án C
            System.out.println("Cau c");
            int sure = JOptionPane.showConfirmDialog(this, "Are you sure??",
                    "Confirm", JOptionPane.YES_NO_OPTION);
            if (sure == JOptionPane.YES_OPTION) {
                MyAudio.playNhac(PLAY_MUSIC, false);
                MyAudio.playNhac(PASS_LEVEL, true);
            } else {
                MyAudio.playNhac(PLAY_MUSIC, false);
                MyAudio.playNhac(LOSE, true);
            }
        } else if (y > 545 && y < 585 && x > 550 && x < 900) { // Đáp án D
            System.out.println("Cau D");
            int sure = JOptionPane.showConfirmDialog(this, "Are you sure??",
                    "Confirm", JOptionPane.YES_NO_OPTION);
            if (sure == JOptionPane.YES_OPTION) {
                MyAudio.playNhac(PLAY_MUSIC, false);
                MyAudio.playNhac(PASS_LEVEL, true);
            } else {
                MyAudio.playNhac(PLAY_MUSIC, false);
                MyAudio.playNhac(LOSE, true);
            }
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
