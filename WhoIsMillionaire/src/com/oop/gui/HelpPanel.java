package com.oop.gui;

import com.oop.gui.utils.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Huy on 7/31/2017.
 */
public class HelpPanel extends JPanel implements Constant, ActionListener, IAudio {
    private IGUI igui;
    private Image background;
    private JButton back;

    public HelpPanel() {
        init();
        addComponent();
    }

    private void addComponent() {
        add(back);
        back.addActionListener(this);
        back.setActionCommand("back");
    }

    private void init() {
        setLayout(null);
        background = ImageUtils.getImage("/image/Backgroud.jpg");
        //tạo icon quay lại start panel
        Icon backbutton = new ImageIcon(getClass().getResource("/image/button_back.png"));
        Image clickedback = ImageUtils.getImage("/image/button_back1.png");
        Icon cliked = new ImageIcon(clickedback.getScaledInstance(backbutton.getIconWidth(), backbutton.getIconHeight(), Image.SCALE_DEFAULT));
        back = new ButtonUtils(backbutton, cliked, cliked, backbutton.getIconWidth()
                , backbutton.getIconHeight(), 35, 35, false);
    }

    public void setIgui(IGUI igui) {
        this.igui = igui;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.drawImage(background, 0, 0, null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String id = e.getActionCommand();
        switch (id) {
            case "back":
                igui.showMenuPanel();
                MyAudio.playNhac(HELP_MUSIC,true);
                break;
        }
    }
}
