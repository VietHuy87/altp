package com.oop;

/**
 * Created by CuongK60UET on 7/26/2017.
 */

import com.oop.developing.question;

import java.sql.*;

public class connectDB {
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/altp";

    //  Database credentials
    private static final String USER = "root";
    private static final String PASS = "cuong123";
    private static Connection conn = null;
    private static Statement stmt = null;

    public static boolean check(String username, String password) {
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT username, password, del_flag FROM account WHERE username = ? and password = ? and del_flag = 0";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            int count = 0;
            while (rs.next()) {
                count++;
            }
            if (count > 0) {
                return true;
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        return false;
    }

    public static boolean addQuestion(question q) {
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = "INSERT INTO question (question, ans_A, ans_B, ans_C, ans_D, correct_ans, level_ans) VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, q.getQuestion());
            ps.setString(2, q.getAns_A());
            ps.setString(3, q.getAns_B());
            ps.setString(4, q.getAns_C());
            ps.setString(5, q.getAns_D());
            ps.setInt(6, q.getCorrect_ans());
            ps.setInt(7, q.getLevel_ans());
            ps.executeUpdate();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        return true;
    }
}//end FirstExample

