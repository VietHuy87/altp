package com.oop;

/**
 * Created by CuongK60UET on 7/30/2017.
 */
public class question {
    private int id;
    private String question;
    private String ans_A;
    private String ans_B;
    private String ans_C;
    private String ans_D;
    private int correct_ans;
    private int level_ans;
    private int del_flag;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public question(String question, String ans_A, String ans_B, String ans_C, String ans_D, int correct_ans, int level_ans) {
        this.question = question;
        this.ans_A = ans_A;
        this.ans_B = ans_B;
        this.ans_C = ans_C;
        this.ans_D = ans_D;
        this.correct_ans = correct_ans;
        this.level_ans = level_ans;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAns_A() {
        return ans_A;
    }

    public void setAns_A(String ans_A) {
        this.ans_A = ans_A;
    }

    public String getAns_B() {
        return ans_B;
    }

    public void setAns_B(String ans_B) {
        this.ans_B = ans_B;
    }

    public String getAns_C() {
        return ans_C;
    }

    public void setAns_C(String ans_C) {
        this.ans_C = ans_C;
    }

    public String getAns_D() {
        return ans_D;
    }

    public void setAns_D(String ans_D) {
        this.ans_D = ans_D;
    }

    public int getCorrect_ans() {
        return correct_ans;
    }

    public void setCorrect_ans(int correct_ans) {
        this.correct_ans = correct_ans;
    }

    public int getLevel_ans() {
        return level_ans;
    }

    public void setLevel_ans(int level_ans) {
        this.level_ans = level_ans;
    }

    public int getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(int del_flag) {
        this.del_flag = del_flag;
    }
}
